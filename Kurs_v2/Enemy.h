#ifndef ENEMY_H
#define ENEMY_H

#include <math.h> 
#include <stdlib.h>
#include <SFML/Graphics.hpp>

#define PI 3.14159265

class Enemy{

private:

	float x,y;
	float speed;
	float dir;
	float health;

	float CurrentFrame;

	float Damage;
	float DamageDelay;
	float ConstDamageDelay;
	
	bool DeadFlag;

	bool CollisionFlag;
	bool DamageFlag;

	sf::Texture EnemyTexture;
	sf::Sprite EnemySprite;
	sf::Color color;
public:

	Enemy(float, float, float);

	float GetX();
	float GetY();

	void SetHealth(float);
	float GetHealth();

	void SetDeadFlag(bool);
	bool GetDeadFlag();

	void SetCollFlag(bool);
	bool GetCollFlag();

	void SetDamageFlag(bool);
	bool GetDamageFlag();

	float EnemyDamage();
	void SetDamageDelay();

	void EnemyUpdate(float, float, float );
	void EnemyDraw(sf::RenderWindow *win);
	float Distance(float, float, float, float);
	bool TestColl(float, float);
};

#endif /* ENEMY_H */