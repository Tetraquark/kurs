#ifndef BUTTON_H
#define BUTTON_H

#include <SFML/Graphics.hpp>
#include <string>

class Button{
private:
	float x,y;
	sf::Font ButtonFont;
	sf::Text ButtonText;
	sf::Vector2i MousePos;
	sf::Color TextColor;

	bool ActiveButton;
public:
	Button(std::string, float, float);
	void DrawButton(sf::RenderWindow *win);
	void UpdateButton();
	bool GetActiveStatus();
	void SetActiveStatus(bool);
};

#endif /* BUTTON_H */