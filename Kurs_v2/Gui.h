#ifndef GUI_H
#define GUI_H

#include <SFML/Graphics.hpp>
#include <string>
#include <sstream>

class Gui{
private:
	int BulletNum;

	sf::RectangleShape Bar_HP;
	sf::RectangleShape Back_Bar_HP;
	sf::RectangleShape Back_Bar;

	sf::Texture HP_Icon_Texture;
	sf::Sprite HP_Icon_Sprite;

	sf::Texture Bullet_Texture;
	sf::Sprite Bullet_Sprite;

	sf::Font TextFont;
	sf::Text TimerText;
	sf::Text KillsText;

public:
	Gui();
	void UpdateGUI(float, int);
	void DrawGUI(sf::RenderWindow *win);
	void SetTimerStr(int, int);
	void SetKillsStr(int);
};
#endif  /* GUI_H */