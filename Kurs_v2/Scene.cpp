#include "Scene.h"


Scene::Scene(){
	settings.antialiasingLevel = 10;
	window.create(sf::VideoMode(XWINSIZE, YWINSIZE), "Coursak", sf::Style::Default, settings);
	window.setFramerateLimit(60);
	
	ActiveScene = 0;
	
	//������������� �������� ����
	FloorTexture.loadFromFile("res/img/floor1.png");
	FloorSprite.setTexture(FloorTexture);
	FloorSprite.setTextureRect(sf::IntRect(0, 0, 128, 128));
	FloorSprite.setPosition(0, 0);

	srand(time(NULL));
}

void Scene::MainCycle(){

	while (window.isOpen()){	
		dt = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		dt = dt/800;
		if (dt > 20) dt = 20; 
		
        sf::Event event;
        while (window.pollEvent(event)){
			if ( event.type == sf::Event::Closed ){
				window.close();
				break;
			}
        }

		switch(ActiveScene){
			case 0: //����� �������� ����
				window.clear();
				DrawFloor();
				MainMenu.DrawScene(&window);

				if( MainMenu.ChangeScene == 1 ){
					ActiveScene = MainMenu.ChangeScene;
					NewGame = new GameLogic(&window);
					delete MainMenu.StartBut;
					delete MainMenu.ExitBut;
				}
				if( MainMenu.ChangeScene == 2 ){
					delete MainMenu.StartBut;
					delete MainMenu.ExitBut;
					window.close();
				}

				window.display();
				break;
			case 1: //����� ����
				NewGame->GameUpdate(dt);
				window.clear();
				DrawFloor();
				NewGame->GameDraw();
				window.display();
				if( NewGame->GetGameOverFlag() ){ 
					GameOver.NewButton();
					GameOver.SetScoreStr(NewGame->GetScore());
					GameOver.SetKillsStr(NewGame->GetKills());
					GameOver.SetTimeStr(NewGame->GetMin(), NewGame->GetSec());
					delete NewGame;
					ActiveScene = 2;
				}
				break;
			case 2: //����� ����� ����
				GameOver.UpdateScene();
				window.clear();
				DrawFloor();
				GameOver.DrawScene(&window);
			
				if( GameOver.BackBut->GetActiveStatus() ){
					ActiveScene = 0;
					MainMenu.ChangeScene = ActiveScene;
					MainMenu.NewButton();
					GameOver.BackBut->SetActiveStatus(false);
					delete GameOver.BackBut;
				}
				window.display();
				break;
		}
    }
}

void Scene::DrawFloor(){ // ����� ��������� ����
	for(int i = 0; i < XWINSIZE/128; i++){
		for(int j = 0; j < YWINSIZE/128; j++){
			FloorSprite.setPosition(i*128, j*128);
			window.draw(FloorSprite);
		}
	}
}
