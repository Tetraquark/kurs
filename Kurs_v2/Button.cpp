#include "Button.h"

Button::Button(std::string NewText, float dx, float dy){
	x = dx; y = dy;

	TextColor = sf::Color::Black;

	ButtonFont.loadFromFile("res/fonts/Candarab.ttf");
	ButtonText.setFont(ButtonFont);
	ButtonText.setString(NewText);
	ButtonText.setCharacterSize(40);
	ButtonText.setColor(TextColor);
	ButtonText.setPosition(x, y);
	
	MousePos = sf::Mouse::getPosition();
	ActiveButton = false;
}

bool Button::GetActiveStatus(){
	return ActiveButton;
}

void Button::SetActiveStatus(bool State){
	ActiveButton = State;
}

void Button::DrawButton(sf::RenderWindow *win){
	win->draw(ButtonText);
	MousePos = sf::Mouse::getPosition(*win);
	UpdateButton();
}

void Button::UpdateButton(){
	sf::Event event;
	float w = ButtonText.getLocalBounds().width;
	float h = ButtonText.getCharacterSize();
	
	if( MousePos.x >= x && MousePos.y >= y && MousePos.x <= x + w && MousePos.y <= y+h ){
		TextColor = sf::Color::White;
		if( sf::Mouse::isButtonPressed(sf::Mouse::Left) ){
			ActiveButton = true;
		}
		else ActiveButton = false;
	}

	else{
		TextColor = sf::Color::Black;
		ActiveButton = false;
	}
	
	ButtonText.setColor(TextColor);
}