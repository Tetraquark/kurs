#ifndef BULLET_H
#define BULLET_H

#include <math.h> 
#include <SFML/Graphics.hpp>
#include <iostream>
#define PI 3.14159265

class Bullet{
private:
	float x,y;
	float speed;
	float dir;
	float TracerLen;

public:
	sf::CircleShape BullShape;

	Bullet(float , float , float);
	float GetX();
	float GetY();
	void BulletUpdate(float); 
	void BulletDraw(sf::RenderWindow *win); //����� ��������� ���� - �����
	~Bullet();
};

#endif /* BULLET_H */