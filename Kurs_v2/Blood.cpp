#include "Blood.h"

Blood::Blood(float Bx, float By){
	x = Bx;
	y = By;

	int BloodType = rand() % 3; 

	//����� ���� ��������
	switch(BloodType){
		case 0:
			BloodTexture.loadFromFile("res/img/blood_0.png");
			break;
		case 1:
			BloodTexture.loadFromFile("res/img/blood_1.png");
			break;
		case 2:
			BloodTexture.loadFromFile("res/img/blood_2.png");
			break;
		default:
			BloodTexture.loadFromFile("res/img/blood_0.png");
			break;
	}

	BloodSprite.setTexture(BloodTexture);
	BloodSprite.setTextureRect( sf::IntRect(0, 0, 128, 128) );
	BloodSprite.setOrigin(16, 16);
	BloodSprite.setScale(0.3, 0.3);
	BloodSprite.setPosition(x, y);
	BloodSprite.setColor(sf::Color::Red);
}

