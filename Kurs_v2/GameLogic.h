#ifndef GAMELOGIC_H
#define GAMELOGIC_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "Player.h"
#include "Bullet.h"
#include "Enemy.h"
#include "Blood.h"
#include "Bonus.h"
#include "Gui.h"

#define YWINSIZE 768
#define XWINSIZE 1024

class GameLogic{
private:

	float dt;
	int Kills;
	int Score;
	bool GameOver; //���� ����� ����

	sf::Clock LiveClock;	//������ ����� ������
	int MinuteTimer;	//������� �����
	int SecondTimer;	//������� ������
	
	std::vector<Bullet*> BulletArr;
	int BullID;
	int BulletCount; 
	bool ShootFlag; 
	int Reloading;

	std::vector<Enemy*> EnemyArr;
	float SpawnX, SpawnY;
	bool SpawnFlag;
	int ReSpawnTime;
	int EnemyCount;
	int EnemyID;

	std::vector<Blood*> BloodArr;

	std::vector<Bonus*> BonusArr;

	Player player;
	Gui gui;

	sf::RenderWindow *window;
public:
	GameLogic(sf::RenderWindow *win);

	int GetScore();
	int GetKills();
	int GetMin();
	int GetSec();
	bool GetGameOverFlag();

	void GameDraw();
	void GameUpdate(float);

	void Shoot();
	void BulletTestCollBord();

	void SpawnEnemy(float, float);
	void UpdateEnemies();
	void DrawEnemies();
	void TestCollEnemies();
	void KillEnemy();

	void GetPlayerDamage();	

	void NewBonus(float, float);
	void TestCollBonus();
	void DrawBonus();

	void TestTimer();
	void DrawTimer();

	~GameLogic();
};

#endif /* GAMELOGIC_H */