#ifndef PLAYER_H
#define PLAYER_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h> 
#include <stdlib.h>

#define PI 3.14159265

class Player{

private:
	float Px, Py; 
	float PlayerSpeed;
	float PlayerAngle;
	float PlayerHealth;

	float MouseX, MouseY;

	int WeaponType;
	int WeaponDamage;
	int AmmoCount;
	int ReloadTime;
	int ShotDelay;
	
	sf::Texture PlayerTexture;
	sf::Sprite PlayerSprite;
	sf::Vector2i MousePos; 

	sf::SoundBuffer PM_Shot_S_Buff;
	sf::SoundBuffer PM_Reload_S_Buff;
	sf::SoundBuffer AK_Shot_S_Buff;
	sf::SoundBuffer Gun_Shot_S_Buff;
	sf::SoundBuffer Gun_Reload_S_Buff;
public:
	sf::Sound Shot_sound;
	sf::Sound Reload_sound;

	Player();

	float GetX();
	float GetY();
	float GetSpeed();
	float GetHealth();
	void SetHealth(float);
	float GetAngle();
	float GetMouseX();
	float GetMouseY();
	int GetAmmo();
	int GetReloadTime();
	int GetWeapotType();
	int GetShootDelay();
	int GetWeapDamage();

	void DrawPlayer(sf::RenderWindow *win);
	void PlayerUpdate(float);
	void SetWeapon(int, float);
};

#endif /* PLAYER_H */