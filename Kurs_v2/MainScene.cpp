#include "MainScene.h"

MainScene::MainScene(){
	Logo_TX.loadFromFile("res/img/logo.png");
	Logo_SP.setTexture(Logo_TX);
	Logo_SP.setPosition(512, 110);
	Logo_SP.setOrigin(96,65);
	angle = 0;
	ChangeScene = 0;

	NewButton();
}

void MainScene::NewButton(){
	StartBut = new Button("Start", 450, 300);
	ExitBut = new Button("Exit", 450, 360);
}

void MainScene::DrawScene(sf::RenderWindow *win){
	angle += 0.1;
	Logo_SP.setRotation(angle);
	win->draw(Logo_SP);

	StartBut->DrawButton(win);
	ExitBut->DrawButton(win);

	UpdateScene();
}

void MainScene::UpdateScene(){
	if( StartBut->GetActiveStatus() ) ChangeScene = 1;

	if( ExitBut->GetActiveStatus() ) ChangeScene = 2;	
}