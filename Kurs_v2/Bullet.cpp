#include "Bullet.h"

Bullet::Bullet(float pl_x, float pl_y, float angle){
	x = pl_x;
	y = pl_y;
	speed = 1.3;
	dir = angle; 
	TracerLen = 30; //����� ��������
}

float Bullet::GetX(){
	return x;
}

float Bullet::GetY(){
	return y;
}

void Bullet::BulletUpdate(float dt){
	x += cos(dir * PI / 180) * speed * dt;
	y += sin(dir * PI / 180) * speed * dt;
}

void Bullet::BulletDraw(sf::RenderWindow *win){ //��������� ����
	sf::Vertex TracerBull[] = {
		sf::Vertex( sf::Vector2f(x, y) ),
		sf::Vertex( sf::Vector2f(x + TracerLen * cos(dir * PI / 180), y + TracerLen * sin(dir * PI / 180)) )
	};
	
	win->draw(TracerBull, 2, sf::Lines);
}

Bullet::~Bullet(){
	std::cout<<"BULL DEL"<<std::endl;
}