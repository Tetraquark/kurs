#ifndef GAMEOVERSCENE_H
#define GAMEOVERSCENE_H

#include <SFML/Graphics.hpp>
#include <string>

#include "MainScene.h"

class GameOverScene : public MainScene{
private:
	sf::Font TextFont;
	sf::Text GameOverText;

	sf::Text ScoreText;
	sf::Text TimeText;
	sf::Text KillsText;
	
public:
	Button *BackBut;

	GameOverScene();
	void UpdateScene();
	void DrawScene(sf::RenderWindow *win);
	void NewButton();

	void SetKillsStr(int);
	void SetTimeStr(int, int);
	void SetScoreStr(int);
};

#endif /* GAMEOVERSCENE */