#include "GameOverScene.h"
#include "Globfunc.h"

GameOverScene::GameOverScene(){
	ChangeScene = 0;
	NewButton();

	TextFont.loadFromFile("res/fonts/Candarab.ttf");

	GameOverText.setFont(TextFont);
	GameOverText.setCharacterSize(60);
	GameOverText.setColor(sf::Color::Black);
	GameOverText.setPosition(400, 10);
	GameOverText.setString("Game Over!");

	ScoreText.setFont(TextFont);
	ScoreText.setCharacterSize(40);
	ScoreText.setColor(sf::Color::Black);
	ScoreText.setPosition(370, 230);
	ScoreText.setString("Your score:");

	TimeText.setFont(TextFont);
	TimeText.setCharacterSize(40);
	TimeText.setColor(sf::Color::Black);
	TimeText.setPosition(370, 280);
	TimeText.setString("Your time:");

	KillsText.setFont(TextFont);
	KillsText.setCharacterSize(40);
	KillsText.setColor(sf::Color::Black);
	KillsText.setPosition(370, 330);
	KillsText.setString("Kills:");

}

void GameOverScene::NewButton(){
	BackBut = new Button("Main Menu", 10, 700);
}

void GameOverScene::DrawScene(sf::RenderWindow *win){
	BackBut->DrawButton(win);
	win->draw(GameOverText);
	win->draw(ScoreText);
	win->draw(TimeText);
	win->draw(KillsText);
}

void GameOverScene::UpdateScene(){
	if( BackBut->GetActiveStatus() ){
		ChangeScene = 0;
	}
}

void GameOverScene::SetKillsStr(int Kills){
	std::string KillsStr = "Kills: " + patch::to_string(Kills);
	KillsText.setString(KillsStr);
}

void GameOverScene::SetTimeStr(int Min, int Sec){
	std::string TimeStr;
	if( Sec < 10 ) TimeStr = "Your time: " + patch::to_string(Min) + ":0" + patch::to_string(Sec);
	else TimeStr = "Your time: " + patch::to_string(Min) + ":" + patch::to_string(Sec);
	TimeText.setString(TimeStr);
}

void GameOverScene::SetScoreStr(int Score){
	std::string ScoreStr = "Your score: " + patch::to_string(Score);
	ScoreText.setString(ScoreStr);
}