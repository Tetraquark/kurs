#ifndef GLOBFUNC_H
#define GLOBFUNC_H

#include <math.h> 
#include <sstream>

namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

#endif /* GLOBFUNC_H */