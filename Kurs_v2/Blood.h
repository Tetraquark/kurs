#ifndef BLOOD_H
#define BLOOD_H

#include <SFML/Graphics.hpp>
#include <stdlib.h>

class Blood{
private:
	float x,y;
	sf::Texture BloodTexture;
public:
	sf::Sprite BloodSprite;
	Blood(float, float);
};

#endif /* BLOOD_H */