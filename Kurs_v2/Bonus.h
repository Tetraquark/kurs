#ifndef BONUS_H
#define BONUS_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <math.h> 

#define PI 3.14159265

class Bonus{
private:

	float x,y;
	float angle;

	int BonusType;
	float HealCount;
	int WeaponType;

	sf::Texture BonusTexture;
public:
	sf::Sprite BonusSprite;

	Bonus(float, float, float);
	void BonusDraw();
	float Distance(float, float, float, float);
	bool TestColl(float, float);
	float GetHeal();
	int GetWeaponType();
	~Bonus();
};

#endif /* BONUS_H */