#include "Gui.h"
#include "Globfunc.h"

Gui::Gui(){

	Back_Bar.setSize(sf::Vector2f(1014.f, 40.f));
	Back_Bar.setFillColor(sf::Color(133,133,133,150));
	Back_Bar.setPosition(5, 5);

	Bar_HP.setSize(sf::Vector2f(200.f, 15.f));
	Bar_HP.setFillColor(sf::Color::Red);
	Bar_HP.setPosition(37, 12);

	Back_Bar_HP.setSize(sf::Vector2f(204.f, 19.f));
	Back_Bar_HP.setFillColor(sf::Color::Black);
	Back_Bar_HP.setPosition(35, 10);

	HP_Icon_Texture.loadFromFile("res/img/medkit.png");
	HP_Icon_Sprite.setTexture(HP_Icon_Texture);
	HP_Icon_Sprite.setPosition(10, 10);

	Bullet_Texture.loadFromFile("res/img/bullet.png");
	Bullet_Sprite.setTexture(Bullet_Texture);

	TextFont.loadFromFile("res/fonts/Candarab.ttf");
	TimerText.setFont(TextFont);
	TimerText.setCharacterSize(20);
	TimerText.setColor(sf::Color::White);
	TimerText.setPosition(600, 10);

	KillsText.setFont(TextFont);
	KillsText.setCharacterSize(20);
	KillsText.setColor(sf::Color::White);
	KillsText.setPosition(770, 10);

}

void Gui::DrawGUI(sf::RenderWindow *win){
	win->draw(Back_Bar);
	win->draw(Back_Bar_HP);
	win->draw(Bar_HP);
	win->draw(HP_Icon_Sprite);
	
	for(int i = 0; i < BulletNum; i++){
		Bullet_Sprite.setPosition(i * 10 + 275, 10);
		win->draw(Bullet_Sprite);
	}

	win->draw(TimerText);
	win->draw(KillsText);
}

void Gui::UpdateGUI(float HP, int NewBulNum){
	sf::Vector2f V_HP(HP * 2, 15.f);
	Bar_HP.setSize(V_HP);
	BulletNum = NewBulNum;
}

void Gui::SetTimerStr(int Min, int Sec){
	std::string TimerStr;
	if(Sec < 10) TimerStr = "Your time: " + patch::to_string(Min) + ":0" + patch::to_string(Sec);
	else TimerStr = "Your time: " + patch::to_string(Min) + ":" + patch::to_string(Sec);
	TimerText.setString(TimerStr);
}

void Gui::SetKillsStr(int Kills){
	std::string KillsStr = "Kills: " + patch::to_string(Kills);
	KillsText.setString(KillsStr);
}

