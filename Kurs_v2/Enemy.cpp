#include "Enemy.h"

Enemy::Enemy(float Fx, float Fy, float dt){
	x = Fx; y = Fy;
	int EnemyType = rand() % 5;

	//����� ���������� �����
	switch(EnemyType){
		case 0:		//����
			speed = 0.1;;
			health = 32;
			Damage = 5;
			ConstDamageDelay = 1 * dt;
			color.r = 255; 
			color.g = 64; 
			color.b = 64;
			break;
		case 1:		//��������
			speed = 0.13;
			health = 48;
			Damage = 1;
			ConstDamageDelay = 0.5 * dt;
			color.r = 64; 
			color.g = 255; 
			color.b = 64;
			break;
		case 2:		//��������
			speed = 0.05;
			health = 128;
			Damage = 10;
			ConstDamageDelay = 2.25 * dt;
			color.r = 64; 
			color.g = 64; 
			color.b = 255;
			break;
		default:	//��������
			speed = 0.09;
			health = 40;
			Damage = 1;
			ConstDamageDelay = 1 * dt;
			color.r = 128; 
			color.g = 128; 
			color.b = 128;
			break;
	}
	
	DamageDelay = ConstDamageDelay;

	DeadFlag = false;
	CollisionFlag = false;
	DamageFlag = true;
	
	//������������� ������� ����������
	EnemyTexture.loadFromFile("res/img/enemy_sprites.png");
	EnemySprite.setTexture(EnemyTexture);
	EnemySprite.setTextureRect( sf::IntRect(0, 0, 34, 34) );
	EnemySprite.setOrigin(17,17);
	EnemySprite.setPosition(x, y);
	EnemySprite.setColor(color);
	CurrentFrame = 0;
}

float Enemy::GetX(){
	return x;
}

float Enemy::GetY(){
	return y;
}

float Enemy::GetHealth(){
	return health;
}

void Enemy::SetHealth(float NewHealth){
	health = NewHealth;
}

void Enemy::SetDeadFlag(bool flag){
	DeadFlag = flag;
}

bool Enemy::GetDeadFlag(){
	return DeadFlag;
}

void Enemy::SetDamageDelay(){
	DamageDelay = ConstDamageDelay;
}

void Enemy::SetCollFlag(bool flag){
	CollisionFlag = flag;
}

bool Enemy::GetCollFlag(){
	return CollisionFlag;
}

void Enemy::SetDamageFlag(bool flag){
	DamageFlag = flag;
}

bool Enemy::GetDamageFlag(){
	return DamageFlag;
}

float Enemy::EnemyDamage(){
	return Damage;
}

void Enemy::EnemyUpdate(float dt, float PlayerX, float PlayerY){
	dir = -atan2((y - PlayerY), (PlayerX - x)) * 180 / PI;
	x += cos(dir * PI / 180) * speed * dt;
	y += sin(dir * PI / 180) * speed * dt;

	if( DamageDelay > 0 ){
		DamageFlag = false;
		DamageDelay--;
	}
	else{
		DamageFlag = true;
	}

	CurrentFrame+= 0.1;
	if( CurrentFrame > 11 ) CurrentFrame = 0;
	EnemySprite.setTextureRect(sf::IntRect(34*int(CurrentFrame), 0, 34, 34));
}
	
void Enemy::EnemyDraw(sf::RenderWindow *win){
	EnemySprite.setPosition(x, y);
	EnemySprite.setRotation(dir);
	win->draw(EnemySprite);
}

float Enemy::Distance(float x0, float y0, float x1, float y1){
	return sqrt( pow(x1-x0, 2) + pow(y1-y0, 2) );
}

bool Enemy::TestColl(float Bx, float By){
	if(Distance(x, y, Bx, By) < 20) return true;
	return false;
}