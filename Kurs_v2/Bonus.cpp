#include "Bonus.h"

Bonus::Bonus(float Bx, float By, float dt){
	int SpriteSize;
	
	x = Bx + 12;
	y = By + 12;
	angle = 3;
	HealCount = 15;

	BonusType = rand() % 6;

	//����� ���� ��������
	switch(BonusType){
		case 0:
			BonusTexture.loadFromFile("res/img/PM.png");
			SpriteSize = 32;
			WeaponType = BonusType;
		break;

		case 1:
			BonusTexture.loadFromFile("res/img/AK.png");
			SpriteSize = 32;
			WeaponType = BonusType;
		break;

		case 2:
			BonusTexture.loadFromFile("res/img/Gun.png");
			SpriteSize = 32;
			WeaponType = BonusType;
		break;

		default: 
			BonusTexture.loadFromFile("res/img/medkit.png");
			SpriteSize = 24;
			WeaponType = -1;
		break;

	}
	BonusType = 0;

	//������������� ������� ������
	BonusSprite.setTexture(BonusTexture);
	BonusSprite.setTextureRect(sf::IntRect(0, 0, SpriteSize, SpriteSize)); 
	BonusSprite.setOrigin(SpriteSize/2,SpriteSize/2);
	BonusSprite.setPosition(x, y);
}

float Bonus::GetHeal(){
	return HealCount;
}

int Bonus::GetWeaponType(){
	return WeaponType;
}

void Bonus::BonusDraw(){ //����� ���������
	angle += 0.2;
	BonusSprite.setRotation(angle);
}

float Bonus::Distance(float x0, float y0, float x1, float y1){ //����� �������� ���������
	return sqrt( pow(x1-x0, 2) + pow(y1-y0, 2) );
}

bool Bonus::TestColl(float Bx, float By){
	if(Distance(x, y, Bx, By) < 20) return true;
	return false;
}

Bonus::~Bonus(){
	std::cout<< "DEL BONUS"<<std::endl;
}