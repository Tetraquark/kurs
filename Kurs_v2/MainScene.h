#ifndef MAINSCENE_H
#define MAINSCENE_H

#include <SFML/Graphics.hpp>

#include "Button.h"

class MainScene{
private:
	float angle;
public:
	int ChangeScene;

	Button *StartBut;
	Button *ExitBut;

	sf::Texture Logo_TX;
	sf::Sprite Logo_SP;	
	
	MainScene();
	void UpdateScene();
	void DrawScene(sf::RenderWindow *win);
	void NewButton();
};

#endif  /* MAINSCENE_H */