#include "Player.h"

Player::Player(){

	Px = 512;
	Py = 384;
	PlayerSpeed = 0.08;
	PlayerAngle = 0;
	PlayerHealth = 100;

	WeaponType = 0;

	//������������� ������� ������
	PlayerTexture.loadFromFile("res/img/player1.png");
	PlayerSprite.setTexture(PlayerTexture);
	PlayerSprite.setTextureRect(sf::IntRect(0, 0, 36, 24));
	PlayerSprite.setOrigin(18,12);
	PlayerSprite.setPosition(Px, Py);
	
	//�������� � ������������� ������
	PM_Shot_S_Buff.loadFromFile("res/sounds/PMd_shot.ogg");
	PM_Reload_S_Buff.loadFromFile("res/sounds/PM_reload.ogg");
	AK_Shot_S_Buff.loadFromFile("res/sounds/AK_shot.ogg");
	Gun_Shot_S_Buff.loadFromFile("res/sounds/Gun_shot.ogg");
	Gun_Reload_S_Buff.loadFromFile("res/sounds/Gun_reload.ogg");

	SetWeapon(WeaponType, 20);
	MousePos = sf::Mouse::getPosition();
}

float Player::GetX(){
	return Px;
}

float Player::GetY(){
	return Py;
}

float Player::GetSpeed(){
	return PlayerSpeed;
}

float Player::GetAngle(){
	return PlayerAngle;
}

float Player::GetMouseX(){
	return MousePos.x;
}
	
float Player::GetMouseY(){
	return MousePos.y;
}

float Player::GetHealth(){
	return PlayerHealth;
}

void Player::SetHealth(float NewHealth){
	PlayerHealth = NewHealth;
}

int Player::GetAmmo(){
	return AmmoCount;
}

int Player::GetReloadTime(){
	return ReloadTime;
}

int Player::GetWeapotType(){
	return WeaponType;
}

int Player::GetShootDelay(){
	return ShotDelay;
}

int Player::GetWeapDamage(){
	return WeaponDamage;
}

void Player::SetWeapon(int Type, float dt){ //����� ��������� ������
	WeaponType = Type;

	switch(WeaponType){
		case 0: //��������
			WeaponDamage = 36;
			AmmoCount = 8;
			ReloadTime = 3 * dt;
			ShotDelay = 1 * dt;
			Shot_sound.setBuffer(PM_Shot_S_Buff);
			Reload_sound.setBuffer(PM_Reload_S_Buff);

			break;
		case 1: //�������
			WeaponDamage = 14;
			AmmoCount = 30;
			ReloadTime = 8 * dt;
			ShotDelay = 0.25 * dt;
			Shot_sound.setBuffer(AK_Shot_S_Buff);
			Reload_sound.setBuffer(PM_Reload_S_Buff);

			break;
		case 2: //��������
			WeaponDamage = 16;
			AmmoCount = 20;
			ReloadTime = 8 * dt;
			ShotDelay = 4 * dt;
			Shot_sound.setBuffer(Gun_Shot_S_Buff);
			Reload_sound.setBuffer(Gun_Reload_S_Buff);
			break;
	}
}

void Player::DrawPlayer(sf::RenderWindow *win){
	PlayerSprite.setRotation(PlayerAngle + 5);
	MousePos = sf::Mouse::getPosition(*win);
	win->draw(PlayerSprite);
}

void Player::PlayerUpdate(float dt){
	PlayerAngle = -atan2((Py - MousePos.y), (MousePos.x - Px)) * 180 / PI;

	if ( sf::Keyboard::isKeyPressed(sf::Keyboard::W)  && Py > 4 ){
		Py-=PlayerSpeed*dt;
	}

	if ( sf::Keyboard::isKeyPressed(sf::Keyboard::S)  && Py < 764 ){
		Py+=PlayerSpeed*dt;
	}

	if ( sf::Keyboard::isKeyPressed(sf::Keyboard::A) && Px > 4 ){
		Px-=PlayerSpeed*dt;
	}

	if ( sf::Keyboard::isKeyPressed(sf::Keyboard::D) && Px < 1020 ){
		Px+=PlayerSpeed*dt;
	}

	PlayerSprite.setPosition(Px, Py);

	if( PlayerHealth > 100 ) PlayerHealth = 100;
	if( PlayerHealth <= 0 ) PlayerHealth = 0;
}
