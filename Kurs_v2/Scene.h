#ifndef SCENE_H
#define SCENE_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <stdlib.h>

#include "GameLogic.h"
#include "MainScene.h"
#include "GameOverScene.h"

#define YWINSIZE 768
#define XWINSIZE 1024

class Scene{
private:
	float dt;
	int ActiveScene;

	sf::Clock clock;
	sf::RenderWindow window; 
	sf::ContextSettings settings;
	sf::Music music;

	sf::Texture FloorTexture;
	sf::Sprite FloorSprite;
	
	GameLogic *NewGame;
	MainScene MainMenu;
	GameOverScene GameOver;

public:
	Scene();
	
	void MainCycle();
	void DrawFloor();
};

#endif  /* SCENE_H */