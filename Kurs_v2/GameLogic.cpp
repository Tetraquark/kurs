#include "GameLogic.h"

GameLogic::GameLogic(sf::RenderWindow *win){
	window = win;

	//������������� ��������� ����������
	BulletCount = 0; 
	ShootFlag = true;
	Reloading = 10;

	SpawnFlag = true;
	ReSpawnTime = 800;
	EnemyCount = 0;

	LiveClock.restart();
	MinuteTimer = 0;
	SecondTimer = 0;
	Score = 0;

	Kills = 0;
	GameOver = false;
}

int GameLogic::GetScore(){
	Score = Kills*10 + SecondTimer + MinuteTimer*60;
	return Score;
}

int GameLogic::GetKills(){
	return Kills;
}

int GameLogic::GetMin(){
	return MinuteTimer;
}

int GameLogic::GetSec(){
	return SecondTimer;
}

bool GameLogic::GetGameOverFlag(){
	return GameOver;
}

void GameLogic::GameDraw(){ //������� ����� ��������� ���� ��������
	for(int i = 0; i < BloodArr.size(); i++){
		window->draw(BloodArr[i]->BloodSprite);
	}

	player.DrawPlayer(window);

	for(int i = 0; i < BulletArr.size(); i++){
		BulletArr[i]->BulletDraw(window);
	}
		
	DrawBonus();
	DrawEnemies();
	gui.DrawGUI(window);
}

void GameLogic::GameUpdate(float dt1){ //������� ����� ���������� ��������
	dt = dt1;

	TestTimer();
	player.PlayerUpdate(dt);
	Shoot();
	BulletTestCollBord();
	TestCollBonus();
	UpdateEnemies();
	GetPlayerDamage();

	if(player.GetHealth() <= 0) GameOver = true;

	gui.UpdateGUI(player.GetHealth(), player.GetAmmo() - BulletCount);
	SecondTimer = LiveClock.getElapsedTime().asSeconds();
	gui.SetTimerStr(MinuteTimer, SecondTimer);
	gui.SetKillsStr(Kills);
}

void GameLogic::Shoot(){ //����� ��������
	if(Reloading > 0){
		ShootFlag = false;
		Reloading--;
	}
	else{
		ShootFlag = true;
	}

	if(BulletCount == player.GetAmmo()){
		player.Reload_sound.play();
		BulletCount = 0;
		Reloading = player.GetReloadTime();
	}

	if (ShootFlag && sf::Mouse::isButtonPressed(sf::Mouse::Left) && player.GetWeapotType() != 2){
		player.Shot_sound.play();
		BulletArr.push_back(new Bullet(player.GetX(), player.GetY(), player.GetAngle() ) );
		BulletCount++;
		Reloading = player.GetShootDelay();
	}

	if (ShootFlag && sf::Mouse::isButtonPressed(sf::Mouse::Left) && player.GetWeapotType() == 2){ //���� ��������
		player.Shot_sound.play();

		int angl = -4;
		while(angl <= 4){
			BulletArr.push_back(new Bullet(player.GetX(), player.GetY(), player.GetAngle() + angl));
			angl+=2;
		}

		BulletCount+=5;
		Reloading = player.GetShootDelay();
	}
}

void GameLogic::BulletTestCollBord(){ //����� �������� ������������ ���� � �������� �����
	for(int i = 0; i < BulletArr.size(); i++){
		BulletArr[i]->BulletUpdate(dt);

		if(	BulletArr[i]->GetX() < -10 || 
			BulletArr[i]->GetX() > XWINSIZE + 10 || 
			BulletArr[i]->GetY() < -10 || 
			BulletArr[i]->GetY() > YWINSIZE + 10)
		{
			delete BulletArr[i];
			BulletArr.erase(BulletArr.begin() + i);
		}
	}
}

void GameLogic::SpawnEnemy(float SpX, float SpY){ //����� �������� ������
	if(ReSpawnTime > 0){
		SpawnFlag = false;
		ReSpawnTime--;
	}
	else{
		SpawnFlag = true;
	}

	if(SpawnFlag){
		EnemyArr.push_back(new Enemy(SpX, SpY, dt)); 
		EnemyCount++;
		ReSpawnTime = 600; 
	}
}

void GameLogic::TestCollEnemies(){ //�������� ������������ ����� � �����
	for(int i = 0; i < BulletArr.size(); i++){
		for(int j = 0; j < EnemyArr.size(); j++){
			if(EnemyArr[j]->TestColl(BulletArr[i]->GetX(), BulletArr[i]->GetY())){
				delete BulletArr[i];
				BulletArr.erase(BulletArr.begin() + i);
				EnemyArr[j]->SetHealth(EnemyArr[j]->GetHealth() - player.GetWeapDamage());
				break;
			}
		}
	}
}

void GameLogic::KillEnemy(){
	int RBloodType = 0;
	for(int i = 0; i < EnemyArr.size(); i++){
		if(EnemyArr[i]->GetHealth() <= 0){		
			BloodArr.push_back( new Blood(EnemyArr[i]->GetX(), EnemyArr[i]->GetY()) );
			if(!EnemyArr[i]->GetCollFlag())  NewBonus(EnemyArr[i]->GetX(), EnemyArr[i]->GetY());
			delete EnemyArr[i];
			EnemyArr.erase(EnemyArr.begin() + i);
			Kills++;
		}
	}
}

void GameLogic::UpdateEnemies(){
	SpawnEnemy(-50, YWINSIZE/2);
	SpawnEnemy(XWINSIZE/2, -50);
	SpawnEnemy(XWINSIZE+50, YWINSIZE/2);
	SpawnEnemy(XWINSIZE/2, YWINSIZE+50);

	for(int i = 0; i < EnemyArr.size(); i++){
		EnemyArr[i]->EnemyUpdate(dt, player.GetX(), player.GetY());
	}
	
	TestCollEnemies();
	KillEnemy();
}

void GameLogic::DrawEnemies(){
	for(int i = 0; i < EnemyArr.size(); i++){
		EnemyArr[i]->EnemyDraw(window);
	}
}

void GameLogic::GetPlayerDamage(){ //��������� ����� ������
	bool TestCollision;
	float NewHealth;

	for(int i = 0; i < EnemyArr.size(); i++){

		TestCollision = EnemyArr[i]->TestColl(player.GetX(), player.GetY());

		if(EnemyArr[i]->GetDamageFlag() && TestCollision){
			NewHealth = player.GetHealth() - EnemyArr[i]->EnemyDamage();
			player.SetHealth(NewHealth);
			EnemyArr[i]->SetDamageDelay();
			printf("HP = %f\n", player.GetHealth());
		}

		if(TestCollision) EnemyArr[i]->SetCollFlag(true);
		else EnemyArr[i]->SetCollFlag(false);
		
	}
}

void GameLogic::TestTimer(){ //����� �������� �����
	if(LiveClock.getElapsedTime().asSeconds() >= 60){
		MinuteTimer++;
		LiveClock.restart();
	}
}

void GameLogic::NewBonus(float Bx, float By){ //����� ��������� ����� �������
	int SpawnBonus = rand() % 5;
	if(SpawnBonus == 0) BonusArr.push_back(new Bonus(Bx, By, dt) );
}

void GameLogic::TestCollBonus(){ //����� �������� ������������ ������ � ��������
	for(int i = 0; i < BonusArr.size(); i++){
		if(BonusArr[i]->TestColl(player.GetX(), player.GetY() )){
			if( BonusArr[i]->GetWeaponType() == -1){
				player.SetHealth( player.GetHealth() + BonusArr[i]->GetHeal());
			}
			else{
				player.SetWeapon(BonusArr[i]->GetWeaponType(), dt);
				Reloading = player.GetShootDelay();
				BulletCount = 0;
			}
			delete BonusArr[i];
			BonusArr.erase(BonusArr.begin() + i);
			printf("Player get bonus\n"); 
		}
	}
}

void GameLogic::DrawBonus(){
	for(int i = 0; i < BonusArr.size(); i++){
		BonusArr[i]->BonusDraw();
		window->draw(BonusArr[i]->BonusSprite);
	}
}

GameLogic::~GameLogic(){
	//for(int i = 0; i < BulletArr.size(); i++){
	//	delete BulletArr[i];
	//}
	BulletArr.clear();
	for(int i = 0; i < EnemyArr.size(); i++){
		delete EnemyArr[i];
	}
	EnemyArr.clear();
	for(int i = 0; i < BloodArr.size(); i++){
		delete BloodArr[i];
	}
	BloodArr.clear();
	for(int i = 0; i < BonusArr.size(); i++){
		delete BonusArr[i];
	}
	BonusArr.clear();
}